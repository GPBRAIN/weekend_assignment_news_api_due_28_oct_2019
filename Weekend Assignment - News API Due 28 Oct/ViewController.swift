//
//  ViewController.swift
//  Weekend Assignment - News API Due 28 Oct
//
//  Created by The App Experts on 25/10/2019.
//  Copyright © 2019 The App Experts. All rights reserved.
//

/*
 
 Specification
 1. The homepage should display a list of all possible categories
 2. Clicking on a category should move to a new view displaying a collection of the most
 recent articles in that category.
 o For each article show the article image, the headline, the date published and
 author.

 3. Clicking on an article should open that article in a WebView
 Requirements
 • The app should be quick and response.
 • Use your judgement on how to display the content
 o You are free to use TableView or CollectionView but justify your reason(s)
 • API Key – You will need to sign up to get an API Key, DO NOT SHARE THEM
 • API - newsapi.org

 Note:
 • Ensure naming conventions & best practices are followed.
 • Follow the specification closely; ensure you include every feature.
 • Test your app rigorously before submission.
 */
 

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

